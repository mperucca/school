package simpledb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.ToIntFunction;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    private final HashMap<Field, Integer> aggregateValues = new HashMap<>();
    private final Function<Tuple, Field> getGroupByFieldValue;
    private final ToIntFunction<Tuple> getTupleValue;
    private final BiFunction<Field, Integer, Integer> aggregator;
    private final TupleDesc tupleDescription;
    private final Iterable<Tuple> tuples;
    
    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */

    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
    	if (gbfield == NO_GROUPING || gbfieldtype == null) {
    		getGroupByFieldValue = tuple -> null;
    		tupleDescription = new TupleDesc(new Type[]{Type.INT_TYPE});
    		tuples = () -> aggregateValues.values().stream().map(intValue -> {
    			Tuple tuple = new Tuple(tupleDescription);
    			tuple.setField(0, new IntField(intValue));
    			return tuple;
    		}).iterator();
    	} else {
    		getGroupByFieldValue = tuple -> tuple.getField(gbfield);
    		tupleDescription = new TupleDesc(new Type[]{gbfieldtype, Type.INT_TYPE});
    		tuples = () -> aggregateValues.entrySet().stream().map(keyAndValue -> {
    			Tuple tuple = new Tuple(tupleDescription);
    			tuple.setField(0, keyAndValue.getKey());
    			tuple.setField(1, new IntField(keyAndValue.getValue()));
    			return tuple;
    		}).iterator();
    	}
        switch (what) {
	        case COUNT:
	        	getTupleValue = tuple -> 1;
	        	aggregator = (field, current) -> aggregateValues.get(field) + 1;
	        	break;
	        case SUM:
	        	getTupleValue = tuple -> ((IntField)tuple.getField(afield)).getValue();
	        	aggregator = (field, current) -> aggregateValues.get(field) + current;
	        	break;
	        case AVG:
	        	getTupleValue = tuple -> ((IntField)tuple.getField(afield)).getValue();
	        	aggregator = new BiFunction<Field, Integer, Integer>() {
					class TotalAndCount {
						final int total, count;
						TotalAndCount(int total, int count) {
							this.total = total;
							this.count = count;
						}
					}
					final HashMap<Field, TotalAndCount> averageStats = new HashMap<>();
					@Override
					public Integer apply(Field field, Integer current) {
						int previous = aggregateValues.get(field);
						TotalAndCount totalAndCount = averageStats.getOrDefault(field, new TotalAndCount(previous, 1));
						int count = totalAndCount.count;
						int oldTotal = totalAndCount.total;
						int newTotal = oldTotal + current;
						int newCount = count + 1;
	        			int newAverage = newTotal / newCount;
	        			averageStats.put(field, new TotalAndCount(newTotal, newCount));
						return newAverage;
					}
				};
	        	break;
	        case MIN:
	        	getTupleValue = tuple -> ((IntField)tuple.getField(afield)).getValue();
	        	aggregator = (field, current) -> Math.min(aggregateValues.get(field), current);
	        	break;
	        case MAX:
	        	getTupleValue = tuple -> ((IntField)tuple.getField(afield)).getValue();
	        	aggregator = (field, current) -> Math.max(aggregateValues.get(field), current);
	        	break;
	    	default:
	    		throw new IllegalArgumentException();
        }
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        Field field = getGroupByFieldValue.apply(tup);
        int value = getTupleValue.applyAsInt(tup);
        aggregateValues.merge(field, value, (previous, current) -> aggregator.apply(field, current));
    }

    /**
     * Create a DbIterator over group aggregate results.
     * 
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
    	return new DbIterator() {
			
    		Iterator<Tuple> tuples = IntegerAggregator.this.tuples.iterator();
    		
			@Override
			public void rewind() throws DbException, TransactionAbortedException {
				tuples = IntegerAggregator.this.tuples.iterator();
			}
			
			@Override
			public void open() throws DbException, TransactionAbortedException {
			}
			
			@Override
			public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
				return tuples.next();
			}
			
			@Override
			public boolean hasNext() throws DbException, TransactionAbortedException {
				return tuples.hasNext();
			}
			
			@Override
			public TupleDesc getTupleDesc() {
				return tupleDescription;
			}
			
			@Override
			public void close() {
			}
		};
    }

}
