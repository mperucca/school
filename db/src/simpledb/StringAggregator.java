package simpledb;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    private final TupleDesc tupleDescription;
    private final Iterable<Tuple> tuples;
    private final Consumer<Tuple> merger;
    
    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        if (what != Op.COUNT) {
        	throw new IllegalArgumentException();
        }
        if (gbfield == NO_GROUPING || gbfieldtype == null) {
    		tupleDescription = new TupleDesc(new Type[]{Type.INT_TYPE});
    		int[] total = {0};
    		merger = tuple -> total[0]++;
    		tuples = () -> {
    			Tuple tuple = new Tuple(tupleDescription);
    			tuple.setField(0, new IntField(total[0]));
    			return Collections.singleton(tuple).iterator();
    		};
    	} else {
    		tupleDescription = new TupleDesc(new Type[]{gbfieldtype, Type.INT_TYPE});
    		HashMap<Field, Integer> aggregateValues = new HashMap<>();
    		merger = tuple -> {
    			Field field = tuple.getField(gbfield);
    			aggregateValues.merge(field, 1, (previous, current) -> previous + 1);
    		};
    		tuples = () -> aggregateValues.entrySet().stream().map(keyAndValue -> {
    			Tuple tuple = new Tuple(tupleDescription);
    			tuple.setField(0, keyAndValue.getKey());
    			tuple.setField(1, new IntField(keyAndValue.getValue()));
    			return tuple;
    		}).iterator();
    	}
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
    	merger.accept(tup);
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public DbIterator iterator() {
    	return new DbIterator() {
			
    		Iterator<Tuple> tuples = StringAggregator.this.tuples.iterator();
    		
			@Override
			public void rewind() throws DbException, TransactionAbortedException {
				tuples = StringAggregator.this.tuples.iterator();
			}
			
			@Override
			public void open() throws DbException, TransactionAbortedException {
			}
			
			@Override
			public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
				return tuples.next();
			}
			
			@Override
			public boolean hasNext() throws DbException, TransactionAbortedException {
				return tuples.hasNext();
			}
			
			@Override
			public TupleDesc getTupleDesc() {
				return tupleDescription;
			}
			
			@Override
			public void close() {
			}
		};
    }

}
