package simpledb;

import java.util.stream.IntStream;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {
	private final int[] hist; //Cumulative Histogram
	private final int MIN, MAX, WIDTH;
	private int total;
    /**
     * Create a new IntHistogram.
     * 
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     * 
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     * 
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't 
     * simply store every value that you see in a sorted list.
     * 
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */
    public IntHistogram(int buckets, int min, int max) {
    	hist = new int[buckets];
    	MIN = min;
    	MAX = max;
    	WIDTH = (offsetFromMin(max) + buckets)/buckets;
    }

    /**
     * Add a value to the set of values that you are keeping a histogram of.
     * @param v Value to add to the histogram
     */
    public void addValue(int v) {
    	hist[bucket(v)]++;
    	total++;
    }
    
    int bucket(int v) {
    	if(v < MIN || v > MAX) {
    		throw new IllegalArgumentException("Attempt to add value out of min/max bound");
    	}
    	return offsetFromMin(v)/WIDTH;
    }
    
    int offsetFromMin(int v) {
    	return v-MIN;
    }

    /**
     * Estimate the selectivity of a particular predicate and operand on this table.
     * 
     * For example, if "op" is "GREATER_THAN" and "v" is 5, 
     * return your estimate of the fraction of elements that are greater than 5.
     * 
     * @param op Operator
     * @param v Value
     * @return Predicted selectivity of this particular operator and value
     */
    public double estimateSelectivity(Predicate.Op op, int v) {
		switch (op) {
			case EQUALS:
				return equalsSelectivity(v);
			case NOT_EQUALS:
				return 1 - equalsSelectivity(v);
			case GREATER_THAN:
				return greaterThanSelectivity(v);
			case GREATER_THAN_OR_EQ:
				return 1 - lessThanSelectivity(v);
			case LESS_THAN:
				return lessThanSelectivity(v);
			case LESS_THAN_OR_EQ:
				return 1 - greaterThanSelectivity(v);
			default:
				throw new IllegalArgumentException("Called estimateSelectivity with predicate " + op);	
		}
    }
    
    double equalsSelectivity(int v) {
    	Partition partition = partition(v);
    	return (double)partition.bucketSize/WIDTH/total;
    }
    
    double lessThanSelectivity(int v) {
    	Partition partition = partition(v);
    	return (partition.leftSize + partition.bucketSize * ((double)estimatedPositionInBucket(v) / WIDTH)) / total;
    }
    
    double greaterThanSelectivity(int v) {
    	Partition partition = partition(v);
    	return ((WIDTH - estimatedPositionInBucket(v) - 1d) / WIDTH * partition.bucketSize + partition.rightSize) / total;
    }
    
    Partition partition(int v) {
    	if (v < MIN) {
    		return new Partition(0, 0, total);
    	}
    	if (v > MAX) {
    		return new Partition(total, 0, 0);
    	}
    	int bucket = bucket(v);
    	int leftBucketValues = IntStream.of(hist).limit(bucket).sum();
    	int bucketSize = hist[bucket];
    	int rightBucketValues = IntStream.range(bucket+1, hist.length).map(i -> hist[i]).sum();
    	return new Partition(leftBucketValues, bucketSize, rightBucketValues);
    }
    
    static class Partition {
    	final int leftSize, bucketSize, rightSize;
    	Partition(int leftSize, int bucketSize, int rightSize) {
    		this.leftSize = leftSize;
    		this.bucketSize = bucketSize;
    		this.rightSize = rightSize;
    	}
    }
    
    int estimatedPositionInBucket(int v) {
    	return offsetFromMin(v) % WIDTH;
    }
    
    /**
     * @return
     *     the average selectivity of this histogram.
     *     
     *     This is not an indispensable method to implement the basic
     *     join optimization. It may be needed if you want to
     *     implement a more efficient optimization
     * */
    public double avgSelectivity()
    {
        return 1.0;
    }
    
    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        return "Histogram with buckets: " + hist.length + ", min: " + MIN + ", max: " + MAX + ", width: " + WIDTH
        		+ " total tuples: " + total + "\n";
        
    }
}
