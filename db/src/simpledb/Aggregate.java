package simpledb;

import java.util.*;

/**
 * The Aggregation operator that computes an aggregate (e.g., sum, avg, max,
 * min). Note that we only support aggregates over a single column, grouped by a
 * single column.
 */
public class Aggregate extends Operator {

    private static final long serialVersionUID = 1L;

    private DbIterator child;
    private final int aggregateField;
    private final int groupByField;
    private final Aggregator.Op aggregatorOperator;
    private final Aggregator aggregator;
    private final DbIterator iterator;
    
    /**
     * Constructor.
     * 
     * Implementation hint: depending on the type of afield, you will want to
     * construct an {@link IntAggregator} or {@link StringAggregator} to help
     * you with your implementation of readNext().
     * 
     * 
     * @param child
     *            The DbIterator that is feeding us tuples.
     * @param afield
     *            The column over which we are computing an aggregate.
     * @param gfield
     *            The column over which we are grouping the result, or -1 if
     *            there is no grouping
     * @param aop
     *            The aggregation operator to use
     */
    public Aggregate(DbIterator child, int afield, int gfield, Aggregator.Op aop) {
    	this.child = child;
    	aggregateField = afield;
    	groupByField = gfield;
    	aggregatorOperator = aop;
    	Type groupByFieldType = gfield == -1 ? null : child.getTupleDesc().getFieldType(gfield);
    	switch (child.getTupleDesc().getFieldType(afield)) {
	    	case INT_TYPE:
	    		aggregator = new IntegerAggregator(gfield, groupByFieldType, afield, aop);
	    		break;
	    	case STRING_TYPE:
	    		aggregator = new StringAggregator(gfield, groupByFieldType, afield, aop);
	    		break;
    		default:
    			throw new IllegalArgumentException();
    	}
    	iterator = aggregator.iterator();
    }

    /**
     * @return If this aggregate is accompanied by a groupby, return the groupby
     *         field index in the <b>INPUT</b> tuples. If not, return
     *         {@link simpledb.Aggregator#NO_GROUPING}
     * */
    public int groupField() {
    	return groupByField;
    }

    /**
     * @return If this aggregate is accompanied by a group by, return the name
     *         of the groupby field in the <b>OUTPUT</b> tuples If not, return
     *         null;
     * */
    public String groupFieldName() {
    	return groupByField == Aggregator.NO_GROUPING ? null : child.getTupleDesc().getFieldName(groupByField);
    }

    /**
     * @return the aggregate field
     * */
    public int aggregateField() {
    	return aggregateField;
    }

    /**
     * @return return the name of the aggregate field in the <b>OUTPUT</b>
     *         tuples
     * */
    public String aggregateFieldName() {
    	return "aggName(" + aggregatorOperator + ") (" + child.getTupleDesc().getFieldName(aggregateField) + ")";
    }

    /**
     * @return return the aggregate operator
     * */
    public Aggregator.Op aggregateOp() {
    	return aggregatorOperator;
    }

    public static String nameOfAggregatorOp(Aggregator.Op aop) {
	return aop.toString();
    }

    public void open() throws NoSuchElementException, DbException,
	    TransactionAbortedException {
    	super.open();
    	child.open();
    	while (child.hasNext()) {
    		aggregator.mergeTupleIntoGroup(child.next());
    	}
    }

    /**
     * Returns the next tuple. If there is a group by field, then the first
     * field is the field by which we are grouping, and the second field is the
     * result of computing the aggregate, If there is no group by field, then
     * the result tuple should contain one field representing the result of the
     * aggregate. Should return null if there are no more tuples.
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
    	return iterator.hasNext() ? iterator.next() : null;
    }

    public void rewind() throws DbException, TransactionAbortedException {
    	iterator.rewind();
    }

    /**
     * Returns the TupleDesc of this Aggregate. If there is no group by field,
     * this will have one field - the aggregate column. If there is a group by
     * field, the first field will be the group by field, and the second will be
     * the aggregate value column.
     * 
     * The name of an aggregate column should be informative. For example:
     * "aggName(aop) (child_td.getFieldName(afield))" where aop and afield are
     * given in the constructor, and child_td is the TupleDesc of the child
     * iterator.
     */
    public TupleDesc getTupleDesc() {
    	TupleDesc tupleDescription = child.getTupleDesc();
    	LinkedList<Type> types = new LinkedList<>();
    	LinkedList<String> names = new LinkedList<>();
    	types.add(tupleDescription.getFieldType(aggregateField));
    	names.add(aggregateFieldName());
    	tupleDescription.getFieldType(aggregateField);
    	if (groupByField != Aggregator.NO_GROUPING) {
    		types.addFirst(tupleDescription.getFieldType(groupByField));
    		names.addFirst(groupFieldName());
    	}
    	return new TupleDesc(types.toArray(new Type[types.size()]), names.toArray(new String[names.size()]));
    }

    public void close() {
    	super.close();
    	child.close();
    }

    @Override
    public DbIterator[] getChildren() {
    	return new DbIterator[]{child};
    }

    @Override
    public void setChildren(DbIterator[] children) {
    	child = children[0];
    }
    
}
