package simpledb;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.StampedLock;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 * 
 * @Threadsafe, all fields are final
 */
public class BufferPool {
    /** Bytes per page, including header. */
    private static final int PAGE_SIZE = 4096;

    private static int pageSize = PAGE_SIZE;
    
    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;

    private final HashMap<PageId, Page> pages = new HashMap<>();
    private final int maxPages;
    private final ConcurrentHashMap<PageId, StampedLock> pageLocks = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<TransactionId, ConcurrentHashMap<PageId, Long>> transactionLocks = new ConcurrentHashMap<>();
    
    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
    	maxPages = numPages;
    }
    
    public static int getPageSize() {
      return pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void setPageSize(int pageSize) {
    	BufferPool.pageSize = pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void resetPageSize() {
    	BufferPool.pageSize = PAGE_SIZE;
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
        throws TransactionAbortedException, DbException {
    	StampedLock pageLock = pageLocks.computeIfAbsent(pid, k -> new StampedLock());
    	ConcurrentHashMap<PageId, Long> thisTransactionLocks = transactionLocks.computeIfAbsent(tid, k -> new ConcurrentHashMap<>());
    	synchronized (tid) {
    		long stamp;
        	Long currentStamp = thisTransactionLocks.get(pid);
        	if (currentStamp != null) {
        		stamp = perm == Permissions.READ_ONLY ?
        				pageLock.tryConvertToReadLock(currentStamp) :
    					pageLock.tryConvertToWriteLock(currentStamp);
        	} else {
        		try {
    				stamp = perm == Permissions.READ_ONLY ?
    						pageLock.tryReadLock(9, TimeUnit.SECONDS) :
    						pageLock.tryWriteLock(9, TimeUnit.SECONDS);
    			} catch (InterruptedException e) {
    				throw new RuntimeException(e);
    			}
        	}
        	if (stamp == 0) {
    			throw new TransactionAbortedException();
    		}
        	thisTransactionLocks.put(pid, stamp);
    	}
    	Page page;
    	synchronized (this) {
	    	page = pages.get(pid);
	    	if (page == null) {
	    		page = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
	    		if (pages.size() == maxPages) {
	    			evictPage();
	    		}
	    		pages.put(pid, page);
	    	}
    	}
    	return page;
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public void releasePage(TransactionId tid, PageId pid) {
    	synchronized (tid) {
    		long stamp = transactionLocks.get(tid).remove(pid);
    		pageLocks.get(pid).unlock(stamp);
    	}
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        transactionComplete(tid, true);
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(TransactionId tid, PageId p) {
        return transactionLocks.getOrDefault(tid, new ConcurrentHashMap<>()).containsKey(p);
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
    	synchronized (tid) {
    		Set<PageId> dirtyPages = transactionLocks.getOrDefault(tid, new ConcurrentHashMap<>()).keySet();
        	if (commit) {
        		flushPages(tid);
        	} else {
        		synchronized (this) {
        			dirtyPages.forEach(this::discardPage);
        		}
        	}
        	transactionLocks.getOrDefault(tid, new ConcurrentHashMap<>()).keySet().forEach(pid -> releasePage(tid, pid));
    	}
    }

    /**
     * Add a tuple to the specified table on behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to and any other 
     * pages that are updated (Lock acquisition is not needed for lab2). 
     * May block if the lock(s) cannot be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        markDirty(Database.getCatalog().getDatabaseFile(tableId).insertTuple(tid, t), tid);
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from and any
     * other pages that are updated. May block if the lock(s) cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction deleting the tuple.
     * @param t the tuple to delete
     */
    public  void deleteTuple(TransactionId tid, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
    	markDirty(Database.getCatalog().getDatabaseFile(t.getRecordId().getPageId().getTableId()).deleteTuple(tid, t), tid);
    }
    
    private synchronized void markDirty(ArrayList<Page> affectedPages, TransactionId tid) {
    	affectedPages.forEach(page -> {
        	page.markDirty(true, tid);
        	pages.put(page.getId(), page);
        });
    }

    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        for (PageId pageId : pages.keySet()) {
        	flushPage(pageId);
        }
    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.
        
        Also used by B+ tree files to ensure that deleted pages
        are removed from the cache so they can be reused safely
    */
    public synchronized void discardPage(PageId pid) {
        pages.remove(pid);
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     */
    private synchronized  void flushPage(PageId pid) throws IOException {
        Page page = pages.get(pid);
        if (page != null) {
	        Database.getCatalog().getDatabaseFile(pid.getTableId()).writePage(page);
	        page.markDirty(false, null);
        }
    }

    /** Write all pages of the specified transaction to disk.
     */
    public synchronized  void flushPages(TransactionId tid) throws IOException {
        for (PageId pagedId : transactionLocks.getOrDefault(tid, new ConcurrentHashMap<>()).keySet()) {
        	flushPage(pagedId);
        }
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized  void evictPage() throws DbException {
        Optional<Page> cleanPage = pages.values().stream().filter(page -> page.isDirty() == null).findAny();
        Page pageToEvict = cleanPage.orElseThrow(() -> new DbException("Cannot evict a page because all are dirty."));
    	try {
			flushPage(pageToEvict.getId());
		} catch (IOException e) {
			throw new RuntimeException("Not sure what to do here...", e);
		}
        discardPage(pageToEvict.getId());
    }

}
