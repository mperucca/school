package simpledb;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

	private final File file;
	private final TupleDesc tupleDescription;
	
    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        file = f;
        tupleDescription = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        return file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        return tupleDescription;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
    	int position = pid.pageNumber() * BufferPool.getPageSize();
    	byte[] bytes = new byte[BufferPool.getPageSize()];
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {
			randomAccessFile.seek(position);
			randomAccessFile.readFully(bytes);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
        try {
			return new HeapPage((HeapPageId)pid, bytes);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
    	int position = page.getId().pageNumber() * BufferPool.getPageSize();
    	byte[] bytes = page.getPageData();
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
			randomAccessFile.seek(position);
			randomAccessFile.write(bytes);
		}
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        return Math.toIntExact(file.length() / BufferPool.getPageSize());
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
    	HeapPage heapPage = getPageWithEmptySlots(tid);
    	heapPage.insertTuple(t);
    	ArrayList<Page> modifiedPages = new ArrayList<>();
		modifiedPages.add(heapPage);
		return modifiedPages;
    }
    
    private HeapPage getPageWithEmptySlots(TransactionId tid) throws TransactionAbortedException, DbException, IOException {
    	int numPages = numPages();
        for (int pageNumber = 0; pageNumber < numPages; pageNumber++) {
        	HeapPageId pageId = new HeapPageId(getId(), pageNumber);
        	BufferPool bufferPool = Database.getBufferPool();
	    	Page page = bufferPool.getPage(tid, pageId, Permissions.READ_WRITE);
	    	HeapPage heapPage = (HeapPage)page;
	    	if (heapPage.getNumEmptySlots() != 0) {
	    		return heapPage;
	    	}
        }
        HeapPageId pageId = new HeapPageId(getId(), numPages);
    	byte[] bytes = new byte[BufferPool.getPageSize()];
    	Path path = file.toPath();
		Files.write(path, bytes, StandardOpenOption.APPEND);
    	return (HeapPage)Database.getBufferPool().getPage(tid, pageId, Permissions.READ_WRITE);
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
    	BufferPool bufferPool = Database.getBufferPool();
    	Page page = bufferPool.getPage(tid, t.getRecordId().getPageId(), Permissions.READ_WRITE);
    	HeapPage heapPage = (HeapPage)page;
    	heapPage.deleteTuple(t);
    	ArrayList<Page> modifiedPages = new ArrayList<>();
		modifiedPages.add(heapPage);
		return modifiedPages;
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
    	
    	Supplier<Iterator<HeapPageId>> init = () -> IntStream.range(0, numPages()).mapToObj(pageNumber -> new HeapPageId(getId(), pageNumber)).iterator();
    	
    	return new DbFileIterator() {
			
    		Iterator<HeapPageId> pageIds = Collections.emptyIterator();
    		Iterator<Tuple> tuples = Collections.emptyIterator();
    		boolean open = false;
    		
			@Override
			public void rewind() throws DbException, TransactionAbortedException {
				if (open) {
					pageIds = init.get();
		    		tuples = Collections.emptyIterator();
				}
			}
			
			@Override
			public void open() throws DbException, TransactionAbortedException {
				if (!open) {
					pageIds = init.get();
					open = true;
				}
			}
			
			@Override
			public Tuple next() throws DbException, TransactionAbortedException,
					NoSuchElementException {
				while (!tuples.hasNext()) {
					nextPage();
				}
				return tuples.next();
			}
			
			@Override
			public boolean hasNext() throws DbException, TransactionAbortedException {
				while (!tuples.hasNext()) {
					if (!pageIds.hasNext()) {
						return false;
					}
					nextPage();
				}
				return true;
			}
			
			@Override
			public void close() {
				pageIds = Collections.emptyIterator();
	    		tuples = Collections.emptyIterator();
	    		open = false;
			}
			
			void nextPage() throws DbException, TransactionAbortedException {
				PageId pageId = pageIds.next();
		    	BufferPool bufferPool = Database.getBufferPool();
		    	Page page = bufferPool.getPage(tid, pageId, Permissions.READ_ONLY);
				tuples = ((HeapPage)page).iterator();
			}
		};
    }

}

